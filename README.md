# Epona AWS HandsOn

## ハンズオン実施後のAWS概略図

このような環境を構築していきます[^1]。

[^1]: GitLabのロゴは[GitLab](https://about.gitlab.com/)によって制作されたものであり、[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)の下に提供されています。

![ハンズオンゴール](./doc/resources/handson-goal.jpg)

各パターンで構築するリソースは図のようになります[^1]。

![パターンとのマッピング](./doc/resources/handson-goal_pattern_mapping_all.jpg)

---

## 事前準備

事前に実施していただく内容は、以下2つです。

* [各種ツールの準備](./doc/manual/requirements.md)
* [事前課題](./doc/manual/assignment.md)

## 当日準備

### Fork

以下2つのリポジトリを利用します。

* [epona-aws-handson](https://gitlab.com/eponas/epona-handson/epona-aws-handson)
* [handson-chat-application](https://gitlab.com/eponas/epona-handson/handson-chat-application)

上記リポジトリをフォークしてください。  
namespaceは自分のアカウントのスペースを選択してください。

### Clone

ForkしたリポジトリをローカルにCloneします。

Windowの方はCドライブ直下にCloneしてください。  
本ハンズオンで扱うリソースのパス名がWindowsの長さ制限を超える場合があるためです。

パーソナルアクセストークンは、[GitLabの準備](doc/manual/gitlab.md) にて発行したトークンを設定してください。

```shell script
$ git clone https://oauth2:[パーソナルアクセストークン]@gitlab.com/[ユーザー名]/handson-chat-application.git
$ git clone https://oauth2:[パーソナルアクセストークン]@gitlab.com/[ユーザー名]/epona-aws-handson.git
```

## 注意事項

ハンズオンで利用するAWS環境ではユーザー操作を記録しています。  
講師の指示がある場合を除いてDevOps環境構築キット以外の操作で **AWSリソースを作成しないでください。**

### 命名ルールについて

１つのAWS環境を講師を含めた複数人で利用するため、命名ルールを設けます。

※１ 他の名前で設定した場合、既存リソースと整合性が取れず、動かないことがあります。  
※２ 通常、環境を共有する場面は少ないと考えてください。

||名前|命名|
|:--|:--|:--|
|1|佐藤|Sato(sato)|
|2|鈴木|Suzuki(suzuki)|
|3|田中|Tanaka(tanaka)|
|4|伊藤|Ito(ito)|
|5|加藤|Kato(kato)|
|6|山田|Yamada(yamada)|
|7|佐々木|Sasaki(sasaki)|
|8|木村|Kimura(kimura)|
|9|森|Mori(mori)|
|10|池田|Ikeda(ikeda)|

例えば、以下のように `[last_name]` と記載されていた場合、佐藤さんなら `sato` と書き替えてください。

```hcl-terraform
  # TODO: name, tags.Ownerの[last_name]を修正
  name = "[last_name]-gitlab-runner"
```

```hcl-terraform
  name = "sato-gitlab-runner"
```

`[LastName]` と記載されていた場合は、 **大文字で始まるようにしてください。**

```hcl-terraform
provider "aws" {
  assume_role {
    # TODO: role_arnのロール名 [LastName]を修正
    role_arn = "arn:aws:iam::938285887320:role/[LastName]TerraformExecutionRole"
  }
}
```

```hcl-terraform
provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/SatoTerraformExecutionRole"
  }
}
```

---

## 構築手順

以下手順で実施していきます。  
なお、時間の都合上、一部手順は講師により実施済みです。

1. [Terraform実行用IAMユーザーと環境の用意 (setup_accounts)](#terraform実行用iamユーザーと環境の用意)
1. [VPCの構築 (network)](#vpcの構築)
1. [ミドルウェアの構築 (database, redis)](#ミドルウェアの構築)
1. [CI環境構築](#ci環境構築)
1. [バックエンドのCD環境構築](#バックエンドのcd環境構築)
   1. バックエンド実施基盤の構築
   1. バックエンドCDパイプラインの構築
1. [フロントエンドのCD環境構築](#フロントエンドのcd環境構築)

### 進め方について

実PJでも受講者が自律的に環境構築できるようになることが重要と考えます。  
よって、本ハンズオンでは以下のように段階を踏むことで、受講者の理解度向上を狙います。

|手順|進め方|目的|
|:---------------|:---------------------------------------------------------|:---------------------------------------------------|
|手順1～3         |受講者により構築を行わない                                    |Eponaをより体験できるコンテンツに注力していただくため        |
|手順4～5-1      |指示有。受講者の手で既存コードを修正し適用する                    |まずは、EponaやTerraformの扱いに慣れていただくため         |
|手順5-2         |指示有。サンプルコードを参照しつつ、受講者の手でコードを書き適用する  |実際の構築手順イメージを持っていただくため                  |

### Terraform実行用IAMユーザーと環境の用意

講師により実施済みです。  
事前にお渡ししているIAMユーザー情報をお使いください。

なお、本手順はEponaのドキュメントに基づいています。

[Epona AWS patternモジュール](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/README.md?ref=v0.1.2)

また、IAMユーザーやDelivery, Runtime環境は以下に格納されています。

|利用するテンプレート|利用目的|格納場所|
|:-------------------------------------------------------------------------------------------------------------------|:-------------------|:----------------------------------------------------------------|
|[Delivery環境構築用](https://gitlab.com/eponas/epona/-/tree/develop/doc/templates/new_envionment/delivery?ref=v0.1.2) |Delivery環境構築のため|`epona-aws-handson/[last_name]/setup_terraform_accounts/delivery`|
|[Runtime環境構築用](https://gitlab.com/eponas/epona/-/tree/develop/doc/templates/new_envionment/runtime?ref=v0.1.2)   |Runtime環境構築のため |`epona-aws-handson/[last_name]/setup_terraform_accounts/runtimes`|

ハンズオンでは解説しませんが、興味がある場合は各自参照可能です。

### VPCの構築

講師により実施済みです。

VPCは以下を使って作成をしています。

|利用するパターン|利用目的|格納場所|
|:-------------------------------------------------------------------------------------------------------|:-----------------------|:-------------------------------------------------------|
|[network pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/network.md?ref=v0.1.2) |Delivery環境のVPC構築のため|`epona-aws-handson/[last_name]/delivery/network`        |
|                                                                                                        |Runtime環境のVPC構築のため |`epona-aws-handson/[last_name]/runtimes/staging/network`|

ハンズオンでは解説しませんが、興味がある場合は各自参照可能です。

### ミドルウェアの構築

講師により実施済みです。

ミドルウェアは以下を使って作成をしています。

|利用するパターン                                                                                                        |利用目的                               |格納場所                                                        |
|:---------------------------------------------------------------------------------------------------------------------|:-----------------------------------|:-------------------------------------------------------------|
|[encryption_key pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/encryption_key.md?ref=v0.1.2) |DBの暗号化のため                      |`epona-aws-handson/[last_name]/runtimes/staging/encryption_key`|
|[database pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/database.md?ref=v0.1.2)             |Amazon RDS構築のため                  |`epona-aws-handson/[last_name]/runtimes/staging/database`     |
|[redis pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/redis.md?ref=v0.1.2)                   |Amazon ElastiCache for Redis構築のため|`epona-aws-handson/[last_name]/runtimes/staging/redis`        |

さらにハンズオンで利用するチャットアプリケーションに必要なリソースも作成しています。

|利用するパターン                                                                                                          |利用目的               |格納場所                                                                  |
|:-----------------------------------------------------------------------------------------------------------------------|:--------------------|:------------------------------------------------------------------------|
|[parameter_store pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/parameter_store.md?ref=v0.1.2) |環境変数の設定のため    |`epona-aws-handson/[last_name]/runtimes/staging/parameter_store`         |
|パターンなし                                                                                                              |Eメール送信機能があるため|`epona-aws-handson/[last_name]/runtimes/staging/temporary_ses_smtp_user` |

#### parameter_store に関する補足

`parameter_store` はAWS Fargate等で動作するアプリケーションに必要な環境変数を秘匿情報として格納できるパターンです。  
Eponaにおいては、秘匿情報は `.tf` ファイルにハードコードするのではなく、入力変数ファイル（`.tfvars` ファイル）に定義することが推奨されています。

[秘匿情報の管理](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/parameter_store.md?ref=v0.1.2#秘匿情報の管理)

秘匿情報はパブリックリポジトリで公開されるべき内容ではありませんが、皆さんの理解の一助となるよう `.tfvars` ファイルも格納しています。  
重要なパスワードなどは伏字としていますので、参考程度に活用してください。

どのような値を設定すべきかについては、以下解説を参照してください。

[入力変数ファイルを作成する](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/parameter_store#入力変数ファイルを作成する)

---

ここまでの手順で作成された環境は以下になります[^1]。

![作成済みの環境](./doc/resources/already_created.jpg)

### CI環境構築

CI環境を整えるためには `ci_pipeline` patternを使っていきます。

構築手順については [こちら](./doc/handson/ci_pipeline.md) に従ってください。

### バックエンドのCD環境構築

バックエンドのCD環境を整えるためには、大きく2つの環境が必要になります。

1. バックエンド実施基盤の構築
   1. アプリケーション（backend, notifier）のデプロイ先の準備
   1. インターネット公開をするためのロードバランサーや証明書、DNSレコードの準備
1. バックエンドCDパイプラインの構築
   1. CIパイプライン実行をトリガーに、デプロイイベントを発火
   1. アプリケーション（backend, notifier）をデプロイする、パイプラインの作成

構築手順については、以下に従ってください。

1. [バックエンド実施基盤の構築](./doc/handson/public_traffic_container_service.md)
1. バックエンドCDパイプラインの構築
   1. [バックエンドイベントトリガーの構築(cd_pipeline_backend_trigger)](./doc/handson/cd_pipeline_backend_trigger.md)
   1. [バックエンドパイプラインの構築(cd_pipeline_backend)](./doc/handson/cd_pipeline_backend.md)

### フロントエンドのCD環境構築

1. フロントエンド実施基盤の構築(cacheable_frontend)
1. フロントエンドパイプラインの構築(cd_pipeline_frontend, cd_pipeline_frontend_trigger)

講師により実施済みです。以下を使って作成をしています。

|利用するパターン                                                                                                                                     |利用目的                                           |格納場所                                                                                        |
|:-------------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------|:----------------------------------------------------------------------------------------------|
|[cacheable_frontend pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/cacheable_frontend.md?ref=v0.1.2)                     |アプリケーションのデプロイ先、公開に向けたLBなどの構築のため|`epona-aws-handson/[last_name]/runtimes/staging/cacheable_frontend`                            |
|[cd_pipeline_frontend pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/cd_pipeline_frontend.md?ref=v0.1.2)                 |パイプライン構築のため                                |`epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_frontend`                          |
|[cd_pipeline_frontend_trigger pattern](https://gitlab.com/eponas/epona/-/blob/master/guide/patterns/aws/cd_pipeline_frontend_trigger.md?ref=v0.1.2) |デプロイイベント構築のため                             |`epona-aws-handson/[last_name]/delivery/runtime_instances/staging/cd_pipeline_frontend_trigger`|

### アプリケーションを触ってみる

全ての環境構築が終了しました。

最後に、アプリケーションを動作確認してみましょう。
以下のURLを、ブラウザで開いてください。

<!-- markdownlint-disable no-bare-urls -->
https://[last_name]-chat-example.staging.epona-handson.com
<!-- markdownlint-enable no-bare-urls -->

Welcome画面が表示されたことを確認できたでしょうか。

<!-- markdownlint-disable no-inline-html -->
<details>
  <summary>上記URLの設定箇所</summary>
<!-- markdownlint-enable no-inline-html -->
受講者の皆さんは、以下のファイルにフロントエンドにアクセスできるURLが記載されています。  
実PJでは、利用するドメインに合わせて以下ファイルの設定を変更してください。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/cacheable_frontend
```

```terraform
  record_name             = "[last_name]-chat-example.staging.epona-handson.com"
```

<!-- markdownlint-disable no-inline-html -->
</details>
<!-- markdownlint-enable no-inline-html -->

このようにEponaを使って、素早くインターネットにアプリケーションを公開する環境を構築出来るようになっています。

せっかくなのでSatoさんが構築したアプリケーションを使って、みんなでチャットしてみましょう！

<!-- markdownlint-disable no-bare-urls -->
URL: https://sato-chat-example.staging.epona-handson.com
<!-- markdownlint-enable no-bare-urls -->

1. `サインアップ` をクリック
1. 以下項目を入力し、 `登録する` を押下
   1. ユーザー名
   1. メールアドレス
   1. パスワード
1. 入力したメールアドレスに確認メールが送信されています。確認してください
1. `ログイン` をクリックし、ログインを行ってください
1. お好きなチャンネルでチャットしてみましょう～

## 補足

ハンズオンで利用するサービス一覧です。  
研修中は解説しませんが、必要に応じて参照してください。

|サービス名|説明|
|:---|:---|
|[Amazon EC2](https://aws.amazon.com/jp/ec2/)|仮想サーバー|
|[Amazon Elastic Container Service (Amazon ECS)](https://aws.amazon.com/jp/ecs/)|クラスター管理されたコンテナアプリケーションの実行・管理サービス|
|[AWS Fargate](https://aws.amazon.com/jp/fargate/)|コンテナの実行サービス|
|[Amazon Elastic Container Registry (ECR)](https://aws.amazon.com/jp/ecr/)|コンテナレジストリ|
|[Amazon Simple Email Service (SES)](https://aws.amazon.com/jp/ses/)|Eメール送受信サービス|
|[Amazon Relational Database Service (Amazon RDS)](https://aws.amazon.com/jp/rds/)|リレーショナルデータベース|
|[Amazon ElastiCache](https://aws.amazon.com/jp/elasticache/)|分散型インメモリデータストア|
|[AWS CodeDeploy](https://aws.amazon.com/jp/codedeploy/)|アプリケーションのデプロイサービス|
|[AWS CodePipeline](https://aws.amazon.com/jp/codepipeline/)|コードのビルド・テスト・デプロイなどの、パイプラインを自動化|
|[Amazon CloudWatch](https://aws.amazon.com/jp/cloudwatch/)|AWSリソースのモニタリングサービス|
|[Amazon Virtual Private Cloud (Amazon VPC)](https://aws.amazon.com/jp/vpc/)|仮想ネットワーク|
|[Amazon Route 53](https://aws.amazon.com/jp/route53/)|DNSサービス|
|[Elastic Load Balancing](https://aws.amazon.com/jp/elasticloadbalancing/)|ネットワークトラフィックの分散|
|[AWS Identity and Access Management (IAM)](https://aws.amazon.com/jp/iam/)|AWSサービス・リソースへの認証・認可管理サービス|
|[AWS Key Management Service (KMS)](https://aws.amazon.com/jp/kms/)|暗号化・署名のための暗号鍵管理サービス|
|[Amazon EventBridge](https://aws.amazon.com/jp/eventbridge/)|イベントバス|
|[Amazon Simple Storage Service (Amazon S3)](https://aws.amazon.com/jp/s3/)|オブジェクトストレージ|
