# Terraformインストール（Linux）

インストール手順を３つ紹介いたします。いずれか実行いただければ問題ありません。

- [バイナリ(zip圧縮)を用いる場合](#バイナリを用いる場合)
- [aptを用いる場合](#aptを用いる場合)
- [tfenvを用いる場合](#tfenvを用いる場合)

versionは2020/12/10現在のEponaが利用するバージョンv0.13.5で記載しています。  
しかし、v0.13.5以上であれば問題ありません。

## バイナリを用いる場合

以下サイトからTerraformのバイナリ(zip圧縮)をダウンロードしてください。

[Download Terraform](https://www.terraform.io/downloads.html)

zipファイルを解凍し、terraformファイルを任意のディレクトリにコピーします。

お好みのコマンドラインツールを起動して、上記ディレクトリにパスを通してください。

```shell script
$ echo $PATH
```

以下が確認できればOKです。

```shell script
$ terraform --version
Terraform v0.13.5
```

## aptを用いる場合

以下サイトの「Linux」タブを参考に、Terraformのインストールをしてください。

[install-terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli#install-terraform)

以下が確認できればOKです。

```shell script
$ terraform --version
Terraform v0.13.5
```

## tfenvを用いる場合

tfenvを利用するとTerraformのバージョン変更が楽になります。

[tfenv installation](https://github.com/tfutils/tfenv#installation)

tfenvがインストールされたことを確認します。

```shell script
$ tfenv -v
```

tfenvを使ってTerraformをインストールします。

```shell script
$ tfenv install 0.13.5
```

どのバージョンを利用するか指定します。

```shell script
$ tfenv use 0.13.5
```

以下が確認できればOKです。

```shell script
$ terraform --version
Terraform v0.13.5
```
