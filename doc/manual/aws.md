# AWSの準備

事前にお渡ししているアカウント情報をご用意ください。  
ここでは、AWSコンソールへログインしていただき、パスワード変更と2要素認証を設定します。

ハンズオンで利用するAWS環境ではユーザー操作を記録しています。  
**ここでの指示内容以外の操作しないでください。**

`Delivery環境` `Runtime環境` それぞれで以下手順を実施してください。

お渡ししている `[AWSコンソール情報]` を利用します。

1. ログインURLにアクセスしてください
1. `IAM ユーザーとしてサインイン` と表示されます。IAMユーザーとパスワードを入力してください
1. 初回アクセスのため、パスワードを再設定して下さい
   1. パスワードの最小文字数は14文字です
   1. 1文字以上のアルファベット大文字 `(A～Z)` を必要とする
   1. 1文字以上のアルファベット小文字 `(a～z)` を必要とする
   1. 少なくとも1つの数字が必要
   1. 少なくとも1つの英数字以外の文字が必要 `(! @ # $ % ^ & * ( ) _ + - = [ ] { } | ')`
1. 変更後、コンソールにログインできます
1. 以下サイトを参考に仮想MFAデバイスの有効化を行って下さい
   1. [仮想 Multi-Factor Authentication (MFA) デバイスの有効化 (コンソール)](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_credentials_mfa_enable_virtual.html)
