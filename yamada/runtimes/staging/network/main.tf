provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/YamadaTerraformExecutionRole"
  }
}

module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.1.2"

  name = "yamada-staging-network"

  tags = {
    Owner              = "yamada"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  cidr_block = "10.240.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.240.1.0/24", "10.240.2.0/24"]
  private_subnets            = ["10.240.3.0/24", "10.240.4.0/24"]
  nat_gateway_deploy_subnets = ["10.240.1.0/24", "10.240.2.0/24"]

  flow_log_enabled = false
}
