provider "aws" {
  assume_role {
    # TODO: role_arnのロール名 [LastName]を修正
    role_arn = "arn:aws:iam::922032444791:role/[LastName]TerraformExecutionRole"
  }
}

module "public_traffic_container_service_backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/public_traffic_container_service?ref=v0.1.2"

  # TODO: nameの[last_name]を修正
  name = "[last_name]-chat-example-backend"

  # TODO: tags.Ownerの[last_name]を修正
  tags = {
    Owner              = "[last_name]"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  vpc_id                             = data.terraform_remote_state.staging_network.outputs.network.vpc_id
  public_subnets                     = data.terraform_remote_state.staging_network.outputs.network.public_subnets
  public_traffic_protocol            = "HTTPS"
  public_traffic_port                = 443
  create_public_traffic_certificate  = true
  public_traffic_inbound_cidr_blocks = ["0.0.0.0/0"]

  # TODO: record_nameの[last_name]を修正
  dns = {
    zone_name   = "epona-handson.com"
    record_name = "[last_name]-chat-example-backend.staging.epona-handson.com"
  }

  container_subnets  = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  container_protocol = "HTTP"
  container_port     = 8080

  container_health_check_path = "/api/health"
  # TODO: [last_name]を修正
  container_cluster_name                = "[last_name]-chat-example-backend"
  container_traffic_inbound_cidr_blocks = ["0.0.0.0/0"]
  container_service_desired_count       = 3
  container_service_platform_version    = "1.4.0"
  container_task_cpu                    = 512
  container_task_memory                 = "1024"

  # TODO: [LastName]を修正　※4行
  default_ecs_task_iam_role_name             = "[LastName]ChatBackendContainerServiceTaskRole"
  default_ecs_task_iam_policy_name           = "[LastName]ChatBackendContainerServiceTaskRolePolicy"
  default_ecs_task_execution_iam_policy_name = "[LastName]ChatBackendContainerServiceTaskExecution"
  default_ecs_task_execution_iam_role_name   = "[LastName]ChatBackendContainerServiceTaskExecutionRole"

  # TODO: [last_name]を修正
  container_log_group_names             = ["[last_name]-ecs-fargate/chat-example-backend"]
  container_log_group_retention_in_days = 90

  # TODO: 以下のJSON内の[last_name]を修正
  container_definitions = <<-JSON
  [
    {
      "name": "[last_name]-chat-example-backend-task",
      "image": "938285887320.dkr.ecr.ap-northeast-1.amazonaws.com/[last_name]-chat-example-backend:latest",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": 8080
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "[last_name]-ecs-fargate/chat-example-backend",
          "awslogs-region": "ap-northeast-1",
          "awslogs-stream-prefix": "backend"
        }
      },
      "secrets": [
        {
          "name": "NABLARCH_DB_URL",
          "valueFrom": "/Yamada/Epona/App/Config/nablarch_db_url"
        },
        {
          "name": "NABLARCH_DB_USER",
          "valueFrom": "/Yamada/Epona/App/Config/nablarch_db_user"
        },
        {
          "name": "NABLARCH_DB_PASSWORD",
          "valueFrom": "/Yamada/Epona/App/Config/nablarch_db_password"
        },
        {
          "name": "NABLARCH_DB_SCHEMA",
          "valueFrom": "/Yamada/Epona/App/Config/nablarch_db_schema"
        },
        {
          "name": "WEBSOCKET_URI",
          "valueFrom": "/Yamada/Epona/App/Config/websocket_url"
        },
        {
          "name": "MAIL_SMTP_HOST",
          "valueFrom": "/Yamada/Epona/App/Config/mail_smtp_host"
        },
        {
          "name": "MAIL_SMTP_PORT",
          "valueFrom": "/Yamada/Epona/App/Config/mail_smtp_port"
        },
        {
          "name": "MAIL_SMTP_USER",
          "valueFrom": "/Yamada/Epona/App/Config/mail_smtp_user"
        },
        {
          "name": "MAIL_SMTP_PASSWORD",
          "valueFrom": "/Yamada/Epona/App/Config/mail_smtp_password"
        },
        {
          "name": "MAIL_FROM_ADDRESS",
          "valueFrom": "/Yamada/Epona/App/Config/mail_from_address"
        },
        {
          "name": "MAIL_RETURNPATH",
          "valueFrom": "/Yamada/Epona/App/Config/mail_returnpath"
        },
        {
          "name": "APPLICATION_EXTERNAL_URL",
          "valueFrom": "/Yamada/Epona/App/Config/application_external_url"
        },
        {
          "name": "CORS_ORIGINS",
          "valueFrom": "/Yamada/Epona/App/Config/cors_origins"
        },
        {
          "name": "NABLARCH_SESSIONSTOREHANDLER_COOKIESECURE",
          "valueFrom": "/Yamada/Epona/App/Config/nablarch_sessionstorehandler_cookiesecure"
        },
        {
          "name": "NABLARCH_LETTUCE_SIMPLE_URI",
          "valueFrom": "/Yamada/Epona/App/Config/nablarch_lettuce_simple_url"
        }
      ]
    }
  ]
  JSON
}
