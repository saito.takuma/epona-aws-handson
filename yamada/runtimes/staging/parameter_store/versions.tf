terraform {
  required_version = ">= 0.13.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.13.0"
    }
  }

  backend "s3" {
    bucket         = "yamada-staging-terraform-tfstate"
    key            = "parameter_store/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "yamada_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/YamadaStagingTerraformBackendAccessRole"
  }
}
