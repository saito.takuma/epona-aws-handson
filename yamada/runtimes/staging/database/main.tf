provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/YamadaTerraformExecutionRole"
  }
}

module "database" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/database?ref=v0.1.2"

  name = "yamada_postgres"

  tags = {
    Owner              = "yamada"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  identifier            = "yamada-rds-instance"
  engine                = "postgres"
  engine_version        = "12.2"
  port                  = 5432
  instance_class        = "db.t3.micro"
  allocated_storage     = 5
  max_allocated_storage = 10

  deletion_protection = false
  skip_final_snapshot = true

  auto_minor_version_upgrade = true
  maintenance_window         = "Tue:18:00-Tue:18:30"
  backup_retention_period    = "0"
  backup_window              = "13:00-14:00" #  (JST)04:00-05:00

  username = "yamada"
  password = "password"

  kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_arn

  vpc_id            = data.terraform_remote_state.staging_network.outputs.network.vpc_id
  availability_zone = "ap-northeast-1a"

  db_subnets           = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  db_subnet_group_name = "yamada-rds-db-subnet-group"

  parameter_group_name   = "yamada-rds-parameter-group"
  parameter_group_family = "postgres12"

  option_group_name                 = "yamada-rds-option-group"
  option_group_engine_name          = "postgres"
  option_group_major_engine_version = "12"

  performance_insights_kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_arn

  # example parameters
  parameters = [
    { name = "max_connections", value = 200, apply_method = "pending-reboot" },
    { name = "shared_buffers", value = 40960, apply_method = "pending-reboot" }
  ]

  option_group_options = []
}
