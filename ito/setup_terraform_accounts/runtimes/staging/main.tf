provider "aws" {
}

# Runtime環境上でのロール
module "terraformer_execution" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/terraform_execution?ref=v0.1.2"

  prefix     = "ito"
  principals = ["arn:aws:iam::938285887320:user/ItoStagingTerraformer"]
}

module "backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/backend?ref=v0.1.2"

  name          = "ito"
  environment   = "Staging"
  force_destroy = false
}
