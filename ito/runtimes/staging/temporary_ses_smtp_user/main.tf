provider "aws" {
  assume_role {
    # 実行権限を定義したロール
    role_arn = "arn:aws:iam::922032444791:role/ItoTerraformExecutionRole"
  }
}

module "smtp_user" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/components/iam/ses_smtp_user?ref=v0.1.2"

  username    = "ItoSesSmtpUser"
  policy_name = "ItoSesSmtpPolicy"

  tags = {
    Owner              = "ito"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
