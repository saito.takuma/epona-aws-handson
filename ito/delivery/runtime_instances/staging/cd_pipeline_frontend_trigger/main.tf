provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/ItoTerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "922032444791"
}

data "aws_region" "current" {}

module "cd_pipeline_frontend_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend_trigger?ref=v0.1.2"

  pipeline_name      = "ito-pipeline-frnt"
  source_bucket_arn  = "arn:aws:s3:::ito-static-resource"
  runtime_account_id = local.runtime_account_id

  # Runtime環境に構築するアーティファクトストアのバケット名
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # 以下は Runtime環境上で cd_pipeline_frontend pattern を動かしてから設定する
  artifact_store_bucket_arn                = "arn:aws:s3:::ito-frnt-artifacts"
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:${data.aws_region.current.name}:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
