provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/SatoTerraformExecutionRole"
  }
}

module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.1.2"

  name = "sato-delivery-network"

  tags = {
    Owner       = "sato"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  cidr_block = "10.250.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.250.1.0/24", "10.250.2.0/24"]
  private_subnets            = ["10.250.3.0/24", "10.250.4.0/24"]
  nat_gateway_deploy_subnets = ["10.250.1.0/24", "10.250.2.0/24"]

  flow_log_enabled = false
}
