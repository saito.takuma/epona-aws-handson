provider "aws" {
  assume_role {
    # TODO: role_arnのロール名 [LastName]を修正
    role_arn = "arn:aws:iam::922032444791:role/TanakaTerraformExecutionRole"
  }
}

module "public_traffic_container_service_notifier" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/public_traffic_container_service?ref=v0.1.2"

  # TODO: nameの[last_name]を修正
  name = "tanaka-chat-example-notifier"

  # TODO: tags.Ownerの[last_name]を修正
  tags = {
    Owner              = "tanaka"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  vpc_id                             = data.terraform_remote_state.staging_network.outputs.network.vpc_id
  public_subnets                     = data.terraform_remote_state.staging_network.outputs.network.public_subnets
  public_traffic_protocol            = "HTTPS"
  public_traffic_port                = 443
  create_public_traffic_certificate  = true
  public_traffic_inbound_cidr_blocks = ["0.0.0.0/0"]

  # TODO: record_nameの[last_name]を修正
  dns = {
    zone_name   = "epona-handson.com"
    record_name = "tanaka-chat-example-notifier.staging.epona-handson.com"
  }

  container_subnets  = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  container_protocol = "HTTP"
  container_port     = 8080

  container_health_check_path = "/api/health"
  # TODO: [last_name]を修正
  container_cluster_name                = "tanaka-chat-example-notifier"
  container_traffic_inbound_cidr_blocks = ["0.0.0.0/0"]
  container_service_desired_count       = 3
  container_service_platform_version    = "1.4.0"
  container_task_cpu                    = 512
  container_task_memory                 = "1024"

  # TODO: [LastName]を修正　※4行
  default_ecs_task_iam_role_name             = "TanakaChatNotifierContainerServiceTaskRole"
  default_ecs_task_iam_policy_name           = "TanakaChatNotifierContainerServiceTaskRolePolicy"
  default_ecs_task_execution_iam_policy_name = "TanakaChatNotifierContainerServiceTaskExecution"
  default_ecs_task_execution_iam_role_name   = "TanakaChatNotifierContainerServiceTaskExecutionRole"

  # TODO: [last_name]を修正
  container_log_group_names             = ["tanaka-ecs-fargate/chat-example-notifier"]
  container_log_group_retention_in_days = 90

  # TODO: [last_name]を修正
  container_definitions = <<-JSON
  [
    {
      "name": "tanaka-chat-example-notifier-task",
      "image": "938285887320.dkr.ecr.ap-northeast-1.amazonaws.com/tanaka-chat-example-ntfr:latest",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": 8080
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "tanaka-ecs-fargate/chat-example-notifier",
          "awslogs-region": "ap-northeast-1",
          "awslogs-stream-prefix": "notifier"
        }
      },
      "secrets": [
        {
          "name": "NABLARCH_LETTUCE_SIMPLE_URI",
          "valueFrom": "/Tanaka/Epona/App/Config/nablarch_lettuce_simple_url"
        }
      ]
    }
  ]
  JSON
}
