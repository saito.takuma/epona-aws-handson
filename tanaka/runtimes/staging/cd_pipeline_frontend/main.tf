provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/TanakaTerraformExecutionRole"
  }
}

module "cd_pipeline_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend?ref=v0.1.2"

  tags = {
    Owner              = "tanaka"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
  delivery_account_id = "938285887320"

  pipeline_name = "tanaka-pipeline-frnt"

  artifact_store_bucket_name = "tanaka-frnt-artifacts"

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_frontend_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::938285887320:role/Tanaka-Pipeline-FrntAccessRole"

  # ci_pipeline の bucket_name と同じ名前を指定する
  source_bucket_name = "tanaka-static-resource"
  source_object_key  = "source.zip"

  # cachable_frontend s3_frontend_bucket_name と同じ名前を指定する
  deployment_bucket_name = "tanaka-chat-example-frontend"
  deployment_object_path = "public"

  deployment_require_approval = false

  cache_invalidation_config = {
    enable                     = true
    cloudfront_distribution_id = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.cloudfront_id
  }
}
