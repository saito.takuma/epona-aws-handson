provider "aws" {
}

# Runtime環境上でのロール
module "terraformer_execution" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/terraform_execution?ref=v0.1.2"

  prefix     = "suzuki"
  principals = ["arn:aws:iam::938285887320:user/SuzukiStagingTerraformer"]
}

module "backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/backend?ref=v0.1.2"

  name          = "suzuki"
  environment   = "Staging"
  force_destroy = false
}
