terraform {
  required_version = ">= 0.13.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.13.0"
    }
  }

  backend "s3" {
    bucket         = "suzuki-staging-terraform-tfstate"
    key            = "database/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "suzuki_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/SuzukiStagingTerraformBackendAccessRole"
  }
}
