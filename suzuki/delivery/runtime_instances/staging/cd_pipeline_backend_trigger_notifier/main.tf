provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/SuzukiTerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "922032444791"
}

data "aws_region" "current" {}

module "cd_pipeline_backend_trigger_notifier" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.1.2"

  name                 = "suzuki-pipeline-ntfr"
  bucket_name          = "suzuki-pipeline-ntfr-source"
  bucket_force_destroy = false
  runtime_account_id   = local.runtime_account_id

  ecr_repositories = {
    "suzuki-chat-example-ntfr" = {
      arn  = "arn:aws:ecr:ap-northeast-1:938285887320:repository/suzuki-chat-example-ntfr"
      tags = ["latest"]
    }
  }

  # Runtime環境上に作成するバケット名をbucket_nameとすると、arn:aws:s3:::bucket_name の形式で記載してください
  # see: https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/s3-arn-format.html
  artifact_store_bucket_arn = "arn:aws:s3:::suzuki-ntfr-artfct"
  target_event_bus_arn      = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
