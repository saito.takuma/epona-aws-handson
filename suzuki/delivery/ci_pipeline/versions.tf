terraform {
  required_version = ">= 0.13.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.13.0"
    }
  }

  backend "s3" {
    # TODO: bucketの先頭[last_name]を修正
    bucket  = "[last_name]-delivery-terraform-tfstate"
    key     = "ci_pipeline/terraform.tfstate"
    encrypt = true
    # TODO: dynamodb_tableの先頭[last_name]を修正
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    # TODO: role_arnのロール名 [LastName]を修正
    role_arn = "arn:aws:iam::938285887320:role/[LastName]DeliveryTerraformBackendAccessRole"
  }
}
