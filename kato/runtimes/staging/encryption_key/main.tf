provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/KatoTerraformExecutionRole"
  }
}

module "encryption_key" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/encryption_key?ref=v0.1.2"

  kms_keys = [
    { alias_name = "alias/kato-common-encryption-key" }
  ]

  tags = {
    Owner              = "kato"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
