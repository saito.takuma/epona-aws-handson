provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/KatoTerraformExecutionRole"
  }
}

module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.1.2"

  name = "kato-delivery-network"

  tags = {
    Owner       = "kato"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  cidr_block = "10.243.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.243.1.0/24", "10.243.2.0/24"]
  private_subnets            = ["10.243.3.0/24", "10.243.4.0/24"]
  nat_gateway_deploy_subnets = ["10.243.1.0/24", "10.243.2.0/24"]

  flow_log_enabled = false
}
